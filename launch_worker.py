import pika

from app import args
from app.database.redis_store import RedisStore
from app.helpers.conf_parser import ConfParser
from app.logger import logger_instance
from app.pika_client.learning_worker import LearningWorker
from app.pika_client.pika_client import QueueType
from app.pika_client.recognizing_worker import RecognizingWorker
from app.pika_client.worker import Worker
from app.recognizer.face_recognizer import FaceRecognizer

logger = logger_instance.get_logger(__name__)


def prepare_worker() -> Worker:
    """Configure worker"""

    config = ConfParser(path=args.config_path)

    redis_store = RedisStore(**config.storage_parameters)
    logger.info(f'Redis adapter for {args.consume_queue_name} has been initialized')

    connection_parameters = pika.ConnectionParameters(**config.connection_parameters)
    connection_parameters.credentials = pika.credentials.PlainCredentials(**config.credentials_parameters)

    model_params = config.model_parameters
    recognizer = FaceRecognizer(model_params.path, redis_store)

    queue_type = QueueType.from_name(args.consume_queue_name)
    common_worker_arguments = {
        'con_par': connection_parameters,
        'consume_queue': args.consume_queue_name,
        'publish_queue': args.publish_queue_name,
        'redis_store': redis_store,
        'recognizer': recognizer,
        'consume_timeout': model_params.consume_timeout
    }

    if queue_type is QueueType.LEARN:
        return LearningWorker(
            **common_worker_arguments,
            model_timeout=model_params.learning_timeout,
        )
    else:
        return RecognizingWorker(
            **common_worker_arguments,
            model_timeout=model_params.recognizing_timeout,
        )


if __name__ == '__main__':
    worker = prepare_worker()
    worker.run()



