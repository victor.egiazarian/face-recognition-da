import redis

from enum import Enum
from typing import Optional

from app.helpers.decorators import singleton
from app.validation.exceptions import DAStoreException


class StorePrefixes(Enum):
    USER_ID = 'user_id:'
    LAST_USER_ID = 'last_user_id'


@singleton
class RedisStore:
    """Redis adapter class"""

    def __init__(self, host: str, port: int, db: int):
        self._redis = redis.StrictRedis(host=host, port=port, db=db)

        if not self._is_key_exist(StorePrefixes.LAST_USER_ID.value):
            self.init_last_user_id()

    @property
    def last_user_id(self) -> Optional[int]:
        """Property to get last user id"""

        return int(self._get_decoded(StorePrefixes.LAST_USER_ID.value))

    def _is_key_exist(self, key: str) -> bool:
        """Check is key exist in the database"""

        return bool(self._redis.get(key))

    def _get_decoded(self, key: str) -> Optional[str]:
        """Wrapper to get rid of decoding every time you get value"""

        value = self._redis.get(key)

        if not value:
            return None

        return value.decode('utf-8')

    def init_last_user_id(self) -> None:
        """Initialize last user id key"""

        self._redis.set(StorePrefixes.LAST_USER_ID.value, 0)

    def clean_by_prefix(self, prefix: Optional[StorePrefixes]) -> None:
        """Remove key-value pairs based on prefix"""

        if prefix is None:
            prefix = ''

        with self._redis.pipeline() as pipe:
            for key in self._redis.scan_iter(f'{prefix.value}*'):
                pipe.delete(key)

            pipe.execute()

    def get_mapped_user_id(self, uuid: str) -> int:
        """Map UUID to internal int id"""

        extended_user_id = f'{StorePrefixes.USER_ID.value}{uuid}'

        if self._is_key_exist(extended_user_id):
            return int(self._get_decoded(extended_user_id))

        self._redis.incrby(StorePrefixes.LAST_USER_ID.value, 1)
        self._redis.set(extended_user_id, self.last_user_id)

        return int(self.last_user_id)

    def get_recognized_user_uuid(self, recognized_id: int) -> str:
        """Get recognized uuid"""

        for key in self._redis.scan_iter(f'{StorePrefixes.USER_ID.value}*'):
            if self._get_decoded(key) == str(recognized_id):
                return key.decode('utf-8').replace(StorePrefixes.USER_ID.value, '')

        raise DAStoreException('There is no users with such a recognized id')

