from app.models.response import SuccessfulResponse, ResponseResult, ErrorResponse
from app.validation.error_types import ErrorMessage, ErrorType


def test_creation_successful_response():
    response = SuccessfulResponse(
        operation_id='op123',
        client_id='cl456',
        result=ResponseResult.TRUE.value
    )
    expected_task_output = '{"operationId": "op123", "clientId": "cl456", "result": "true"}'

    assert response.to_json() == expected_task_output


def test_creation_error_response():
    response = ErrorResponse(
        operation_id='op123',
        client_id='cl456',
        error=ErrorMessage(
            type=ErrorType.CONVERSION.value,
            message='Wrong encoding'
        )
    )
    expected_task_output = '{"operationId": "op123", "clientId": "cl456", ' \
                           '"error": {"type": "CONVERSION_ERROR", "message": "Wrong encoding"}}'

    assert response.to_json() == expected_task_output
