import pytest

from collections import namedtuple

from app.models.request import Request
from app.validation.exceptions import DAConversionException


ExceptionCase = namedtuple('ExceptionCase', ['task', 'expected_exc_message'])


def test_creation_from_task():
    task = '{"operationId": "op123", "clientId": "cl456", "number": 1, "total": 5, "data": "iVBORw0KG"}'.encode('utf-8')
    expected_request = Request(
        operation_id='op123',
        client_id='cl456',
        number=1,
        total=5,
        data='iVBORw0KG'
    )

    assert Request.from_task(task) == expected_request


@pytest.mark.parametrize('task, expected_exc_message', [
    ExceptionCase(
        task='{"operationId": "op_id123", "number": 1, "total": 5, "data": "iVBORw0KGgoAA"}'.encode('utf-8'),
        expected_exc_message='missing 1 required positional argument: \'client_id\''
    ),
    ExceptionCase(
        task='{"operationId": "op123", "clientId": "cl456", "number": 1, "total": 5, "data": "iVBORw0KG"}'.encode(
            'utf-16'),
        expected_exc_message='Wrong encoding'
    ),
    ExceptionCase(
        task='"operationId": "op123", "clientId": "cl456", "number": 1, "total": 5, "data": "iVBORw0KG"}'.encode(
            'utf-8'),
        expected_exc_message='Wrong JSON structure'
    ),
])
def test_raising_exceptions_with_message(task: bytes, expected_exc_message: str):
    with pytest.raises(DAConversionException) as exc_info:
        Request.from_task(task)

    assert expected_exc_message in str(exc_info.value)
