from collections import namedtuple
from configparser import ConfigParser, NoSectionError
from os.path import isfile
from typing import Dict

from app.helpers.decorators import singleton, with_exception_check
from app.validation.exceptions import DAConfigException

ConnectionParameters = namedtuple('ConnectionParameters', ['host', 'port', 'virtual_host'])
CredentialsParameters = namedtuple('CredentialsParameters', ['username', 'password'])
StorageParameters = namedtuple('StorageParameters', ['host', 'port', 'db'])
ModelParameters = namedtuple(
    'ModelParameters', ['path', 'learning_timeout', 'recognizing_timeout', 'consume_timeout']
)


@singleton
class ConfParser:
    """Config parser class"""

    def __init__(self, path: str):
        self._config_parser = ConfigParser()
        self._config_parser.read(path)

        if not isfile(path):
            raise DAConfigException(f'Config by {path} was not found')

    @property
    @with_exception_check(check=[NoSectionError, ValueError], raise_it=DAConfigException)
    def connection_parameters(self, section='connection_parameters') -> Dict:
        """Extract connection parameters"""

        host = self._config_parser.get(section, 'HOST')
        port = self._config_parser.getint(section, 'PORT')
        vhost = self._config_parser.get(section, 'VHOST')

        return ConnectionParameters(host=host, port=port, virtual_host=vhost)._asdict()

    @property
    @with_exception_check(check=[NoSectionError, ValueError], raise_it=DAConfigException)
    def credentials_parameters(self, section='credentials_parameters') -> Dict:
        """Extract RMQ credentials"""

        username = self._config_parser.get(section, 'USERNAME')
        password = self._config_parser.get(section, 'PASSWORD')

        return CredentialsParameters(username=username, password=password)._asdict()

    @property
    @with_exception_check(check=[NoSectionError, ValueError], raise_it=DAConfigException)
    def storage_parameters(self, section='storage') -> Dict:
        """Extract Redis parameters"""

        host = self._config_parser.get(section, 'HOST')
        port = self._config_parser.getint(section, 'PORT')
        db = self._config_parser.getint(section, 'DB')

        return StorageParameters(host=host, port=port, db=db)._asdict()

    @property
    @with_exception_check(check=[NoSectionError, ValueError], raise_it=DAConfigException)
    def model_parameters(self, section='model') -> ModelParameters:
        """Extract model parameters"""

        path = self._config_parser.get(section, 'PATH')
        learning_timeout = self._config_parser.getfloat(section, 'LEARNING_TIMEOUT')
        recognizing_timeout = self._config_parser.getfloat(section, 'RECOGNIZING_TIMEOUT')
        consume_timeout = self._config_parser.getfloat(section, 'CONSUME_TIMEOUT')

        return ModelParameters(path=path, learning_timeout=learning_timeout,
                               recognizing_timeout=recognizing_timeout, consume_timeout=consume_timeout)
