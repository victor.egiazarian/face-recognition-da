import os


def create_dir(path: str) -> None:
    """Create the dir or check its existence"""

    if not os.path.exists(path):
        try:
            os.mkdir(path)
        except OSError:
            raise OSError(f'Creation of the directory {path} failed')
