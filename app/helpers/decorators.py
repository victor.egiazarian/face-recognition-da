import functools
import time
from typing import Callable, Any, Type, List

from app.logger import logger_instance
from app.validation.exceptions import DABaseException

logger = logger_instance.get_logger(__name__)


def timing(target_function: Callable) -> Callable:
    """Measure target function invocation time"""

    @functools.wraps(target_function)
    def wrapper(*args, **kwargs):
        start = time.time()
        result = target_function(*args, **kwargs)
        end = time.time()
        logger.info(f'Elapsed time for {target_function.__name__}: {end - start}')

        return result
    return wrapper


def singleton(target_class: Any) -> Callable:
    """Make class to be singleton"""

    instance = None

    @functools.wraps(target_class)
    def wrapper(*args, **kwargs):
        nonlocal instance
        if instance is None:
            instance = target_class(*args, **kwargs)
        return instance

    return wrapper


def with_exception_check(target_function=None, *, check: List[Type[Exception]], raise_it: Type[DABaseException]):
    """Run function with exception check"""

    if target_function is None:
        return lambda call: with_exception_check(call, check=check, raise_it=raise_it)

    @functools.wraps(target_function)
    def wrapper(*args, **kwargs):
        try:
            result = target_function(*args, **kwargs)
        except tuple(check) as e:
            raise raise_it(e)
        else:
            return result

    return wrapper

