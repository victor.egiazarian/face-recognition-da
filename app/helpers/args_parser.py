import argparse
from enum import Enum

DEFAULT_CONF_PATH = './conf/launch_worker.ini'


class CliArgs(Enum):
    CONF_PATH = '--config_path'
    CONSUME_QUEUE = '--consume_queue_name'
    PUBLISH_QUEUE = '--publish_queue_name'
    LOGS_PATH = '--logs_path'


def parse_cli_args() -> argparse.Namespace:
    """Parse cli arguments"""

    parser = argparse.ArgumentParser()

    parser.add_argument(CliArgs.CONF_PATH.value, type=str, required=False, default=DEFAULT_CONF_PATH)
    parser.add_argument(CliArgs.CONSUME_QUEUE.value, type=str, required=False)
    parser.add_argument(CliArgs.PUBLISH_QUEUE.value, type=str, required=False)
    parser.add_argument(CliArgs.LOGS_PATH.value, type=str, required=False)

    return parser.parse_args()
