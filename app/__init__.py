import os

from app.helpers.args_parser import parse_cli_args
from app.helpers.utils import create_dir
from app.logger import logger_instance

args = parse_cli_args()

log_dir, _log_name = os.path.split(args.logs_path)
create_dir(log_dir)
logger_instance.set_logs_path(args.logs_path)
