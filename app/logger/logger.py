import logging
import sys
from logging.handlers import TimedRotatingFileHandler


class Logger:
    """Custom logger class"""

    LOG_FORMATTER = logging.Formatter("%(asctime)s %(name)s [%(levelname)s] %(message)s")
    LOG_ROTATION = 'midnight'

    def __init__(self, level=logging.DEBUG, propagate=False):
        self._level = level
        self._propagate = propagate
        self._logs_path = None

    def set_logs_path(self, logs_path: str):
        """Set logs path"""

        self._logs_path = logs_path

    def get_logger(self, logger_name: str) -> logging.Logger:
        """Get configured logger instance"""

        if self._logs_path is None:
            raise ValueError('No logs path has been set')

        logger = logging.getLogger(logger_name)

        logger.setLevel(self._level)
        logger.addHandler(self._get_console_handler())
        logger.addHandler(self._get_file_handler())
        logger.propagate = self._propagate

        return logger

    def _get_console_handler(self) -> logging.StreamHandler:
        """Configure console output"""

        console_handler = logging.StreamHandler(sys.stdout)
        console_handler.setFormatter(self.LOG_FORMATTER)

        return console_handler

    def _get_file_handler(self) -> logging.FileHandler:
        """Configure log file output"""

        file_handler = TimedRotatingFileHandler(self._logs_path, when=self.LOG_ROTATION)
        file_handler.setFormatter(self.LOG_FORMATTER)

        return file_handler

