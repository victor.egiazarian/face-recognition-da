import re


def convert_camel_case_to_snake(camel_case_name: str) -> str:
    """Convert camel case string to snake case one"""

    return re.sub(r'(?<!^)(?=[A-Z])', '_', camel_case_name).lower()


def convert_snake_case_to_camel(snake_case_name: str) -> str:
    """Convert snake case string to camel case one"""

    capitalized_camel = ''.join(word.title() for word in snake_case_name.split('_'))

    return capitalized_camel[0].lower() + capitalized_camel[1:]

