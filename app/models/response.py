import json
from dataclasses import asdict, dataclass
from enum import Enum

from app.models.request import Request
from app.models.utils import convert_snake_case_to_camel
from app.validation.error_types import ErrorMessage
from app.validation.exceptions import DABaseException


class ResponseResult(Enum):
    TRUE = 'true'
    FALSE = 'false'
    NULL = 'null'


@dataclass
class Response:
    """DTO for RabbitMQ response message"""

    operation_id: str
    client_id: str

    def to_json(self) -> str:
        """Serializes object to json"""

        camel_cased_object = {convert_snake_case_to_camel(key): value for (key, value) in asdict(self).items()}

        return json.dumps(camel_cased_object)

    def __str__(self) -> str:
        """Change Response string representation"""

        return str(asdict(self))


@dataclass
class SuccessfulResponse(Response):
    """DTO for successful response message"""

    result: ResponseResult


@dataclass
class ErrorResponse(Response):
    """DTO for error response message"""

    error: ErrorMessage

    @classmethod
    def from_exception(cls, request: Request, exc: DABaseException) -> 'Response':
        """Extra constructor to create error response from based on exception"""

        return cls(
            operation_id=request.operation_id,
            client_id=request.client_id,
            error=ErrorMessage(type=exc.error_type.value, message=exc.message)
        )
