import json
from dataclasses import dataclass, asdict
from json.decoder import JSONDecodeError

from app.models.utils import convert_camel_case_to_snake
from app.validation.exceptions import DAConversionException


@dataclass
class Request:
    """DTO for RabbitMQ request message"""

    operation_id: str
    client_id: str
    number: int
    total: int
    data: str

    @classmethod
    def _convert_camel_keys(cls, **kwargs) -> 'Request':
        """Convert json keys to dictionary ones"""

        return cls(**{convert_camel_case_to_snake(key): value for (key, value) in kwargs.items()})

    @classmethod
    def from_dict(cls, **kwargs) -> 'Request':
        """Extra constructor for creation object based on json dict"""

        try:
            snake_cased_object = cls._convert_camel_keys(**kwargs)
        except (TypeError, ValueError) as e:
            raise DAConversionException(e)
        else:
            return snake_cased_object

    @classmethod
    def from_json(cls, json_object: str) -> 'Request':
        """Extra constructor for creation Request based on json"""

        try:
            dict_object = json.loads(json_object)
        except JSONDecodeError:
            raise DAConversionException('Wrong JSON structure')
        else:
            return cls.from_dict(**dict_object)

    @classmethod
    def from_task(cls, task_body: bytes) -> 'Request':
        """Extra constructor for creation Request based on bytes task"""

        try:
            task_body = task_body.decode('utf8')
        except ValueError:
            raise DAConversionException('Wrong encoding')
        else:
            return cls.from_json(task_body)

    def is_completed(self) -> bool:
        """Check is learning completed"""

        return self.number == self.total

    def __str__(self) -> str:
        """Change Request string representation"""

        representation_size = 10

        dict_request = asdict(self)
        short_data = dict_request.get('data')[:representation_size]
        dict_request['data'] = f'{short_data}...'

        return str(dict_request)
