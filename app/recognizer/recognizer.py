import abc
import numpy as np


class Recognizer(metaclass=abc.ABCMeta):
    """Abstract class for recognizer"""

    @abc.abstractmethod
    def train_recognizer(self, image: np.ndarray, label: int) -> bool:
        """To train recognizer"""

        pass

    @abc.abstractmethod
    def recognize(self, image: np.ndarray) -> int:
        """Recognize image"""

        pass
