import base64
import os
from typing import List

import cv2
import numpy as np

from app.database.redis_store import RedisStore, StorePrefixes
from app.logger import logger_instance
from app.helpers.decorators import timing, singleton
from app.helpers.utils import create_dir
from app.recognizer.recognizer import Recognizer

logger = logger_instance.get_logger(__name__)


def image_to_numpy_array(image: str) -> np.ndarray:
    """Convert input image to numpy array"""

    bytes_image = base64.b64decode(image)
    np_image = np.frombuffer(bytes_image, dtype=np.uint8)

    return cv2.imdecode(np_image, flags=1)


@singleton
class FaceRecognizer(Recognizer):
    """Face recognizer class"""

    def __init__(self, model_path: str, redis_store: RedisStore):
        self.model_path = model_path

        model_dir = '/'.join(model_path.split('/')[:-1])

        if not os.path.isfile(model_path):
            create_dir(model_dir)

            redis_store.clean_by_prefix(StorePrefixes.USER_ID)
            redis_store.init_last_user_id()

            self._create_recognizer()

    def _create_recognizer(self) -> None:
        """Create recognizer"""

        recognizer = cv2.face.LBPHFaceRecognizer_create()
        recognizer.save(self.model_path)
        logger.info(f'New model in the {self.model_path} has been created')

    @timing
    def train_recognizer(self, image: np.ndarray, label: int) -> bool:
        """Train recognizer stored by self.model_path"""

        recognizer = cv2.face.LBPHFaceRecognizer_create()
        recognizer.read(self.model_path)

        gs_image: np.ndarray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        faces: List[np.ndarray] = [gs_image]
        labels: np.ndarray = np.array([label])

        recognizer.update(faces, labels)
        recognizer.save(self.model_path)

        return True

    @timing
    def recognize(self, image: np.ndarray) -> int:
        """Use recognizer stored by self.model_path"""

        recognizer = cv2.face.LBPHFaceRecognizer_create()
        recognizer.read(self.model_path)

        gs_image: np.ndarray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        return recognizer.predict(gs_image)


