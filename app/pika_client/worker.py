import abc
import time
from collections import deque
from functools import partial
from typing import List, Optional, Dict, Deque, Set

from pika import ConnectionParameters
from pika.channel import Channel
from pika.spec import Basic, BasicProperties

from app.database.redis_store import RedisStore
from app.logger import logger_instance
from app.models.request import Request
from app.models.response import Response, ErrorResponse
from app.pika_client.pika_client import PikaClient
from app.recognizer.face_recognizer import FaceRecognizer
from app.validation.error_types import ErrorMessage, ErrorType
from app.validation.exceptions import DATimeoutException

logger = logger_instance.get_logger(__name__)


class Worker(PikaClient, metaclass=abc.ABCMeta):
    """Base Worker class"""

    CHECK_RESPONSE_AWAIT_TIME = 0.1
    HISTORY_CACHE_SIZE = 50

    def __init__(
            self,
            con_par: ConnectionParameters,
            consume_queue: str,
            publish_queue: str,
            exchange: str,
            redis_store: RedisStore,
            recognizer: FaceRecognizer,
            model_timeout: float,
            consume_timeout: float

    ):
        super().__init__(
            con_par=con_par,
            publish_queue=publish_queue,
            consume_queue=consume_queue,
            exchange=exchange,
            model_timeout=model_timeout,
            consume_timeout=consume_timeout
        )

        self._recognizer = recognizer
        self._redis_store = redis_store

        self._responses: List[Response] = []
        self._processing_tasks: Set[str] = set()
        self._recently_removed_tasks: Deque[str] = deque(maxlen=self.HISTORY_CACHE_SIZE)
        self._last_received_requests: Dict[str, float] = {}

    @abc.abstractmethod
    def handle(self, request: Request) -> None:
        """Handle operations with opencv model"""

        pass

    def on_message(
            self,
            _channel: Channel,
            _basic_deliver: Basic.Deliver,
            _properties: BasicProperties,
            body: bytes
    ) -> None:
        """Invoked when there is a message in consuming queue"""

        request = Request.from_task(body)
        operation_id = request.operation_id

        if operation_id in self._recently_removed_tasks:
            self.publish_response(ErrorResponse(
                operation_id=request.operation_id,
                client_id=request.client_id,
                error=ErrorMessage(type=ErrorType.QUEUE.value, message='Operation id should be unique for each task')
            ))

            return

        logger.info(f'Received request: {request}')

        try:
            self._check_consume_timeout(request)
        except DATimeoutException as e:
            self._responses.append(ErrorResponse.from_exception(request=request, exc=e))

        if operation_id not in self._processing_tasks:
            self._processing_tasks.add(operation_id)
            self._try_to_publish_response(operation_id)

        self.handle(request)

    def _try_to_publish_response(self, operation_id: str) -> None:
        """Try to publish prepared response"""

        response = self._try_to_extract_response(operation_id)

        if response is not None:
            self.publish_response(response)
        else:
            publish_callback = partial(self._try_to_publish_response, operation_id)
            self._connection.ioloop.call_later(self.CHECK_RESPONSE_AWAIT_TIME, publish_callback)

    def _try_to_extract_response(self, operation_id: str) -> Optional[Response]:
        """Get response by operation id and remove all related tasks"""

        target_response_index: Optional[int] = None

        for index, response in enumerate(self._responses):
            if operation_id == response.operation_id:
                target_response_index = index
                break

        if target_response_index is None:
            return

        self._processing_tasks.discard(operation_id)
        self._recently_removed_tasks.append(operation_id)

        return self._responses.pop(target_response_index)

    def _check_consume_timeout(self, request: Request) -> None:
        """Check queue consuming timeout"""

        operation_id = request.operation_id

        if request.is_completed():
            if operation_id in self._last_received_requests:
                del self._last_received_requests[operation_id]

            return

        if operation_id in self._last_received_requests:
            previous_request_time = self._last_received_requests.get(operation_id)
            current_request_time = time.time()

            if current_request_time - previous_request_time >= self._consume_timeout:
                del self._last_received_requests[operation_id]

                raise DATimeoutException(f'Consuming timeout has been reached for {request}')

        self._last_received_requests[operation_id] = time.time()
