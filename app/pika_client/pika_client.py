import abc
import asyncio
import concurrent
import multiprocessing
from concurrent.futures.thread import ThreadPoolExecutor
from enum import Enum, auto
from typing import Optional, Callable, Any

import pika
from pika import ConnectionParameters, SelectConnection, BasicProperties
from pika.channel import Channel
from pika.exceptions import AMQPError
from pika.spec import Basic

from app.logger import logger_instance
from app.models.response import Response, SuccessfulResponse
from app.validation.exceptions import DATimeoutException

logger = logger_instance.get_logger(__name__)


class QueueType(Enum):
    LEARN = auto()
    RECOGNIZE = auto()

    @classmethod
    def from_name(cls, queue_name: str) -> 'QueueType':
        """Get queue type based on its name"""

        return cls.LEARN if 'learning' in queue_name else cls.RECOGNIZE


class PikaClient(metaclass=abc.ABCMeta):
    """Async RabbitMQ client"""

    def __init__(
            self,
            con_par: ConnectionParameters,
            consume_queue: str,
            publish_queue: str,
            exchange: str,
            model_timeout: float,
            consume_timeout: float
    ):
        self._consume_queue = consume_queue
        self._publish_queue = publish_queue
        self._con_par = con_par
        self._exchange = exchange

        self._connection: Optional[SelectConnection] = None
        self._channel: Optional[Channel] = None
        self._consumer_tag: Optional[str] = None

        self._model_timeout = model_timeout
        self._consume_timeout = consume_timeout

    @abc.abstractmethod
    def on_message(
            self,
            _channel: Channel,
            _basic_deliver: Basic.Deliver,
            _properties: BasicProperties,
            body: bytes
    ) -> None:
        """Invoked when there is a message in consuming queue"""

        pass

    def connect(self) -> SelectConnection:
        """Connect to RabbitMQ, returning the connection handle"""

        return pika.SelectConnection(parameters=self._con_par,
                                     on_open_callback=self.on_connection_open,
                                     on_close_callback=self.on_connection_closed)

    def on_connection_open(self, _connection: SelectConnection) -> None:
        """Invoked when the connection established"""

        logger.info(f'Connection has been established {self._con_par}')
        self._connection.channel(on_open_callback=self.on_channel_open)

    def on_connection_closed(self, _connection: SelectConnection, reply_code: int) -> None:
        """Invoked when the connection closed unexpectedly"""

        logger.info(f'Connection has been closed with code: {reply_code}')
        self._channel = None
        self._connection.ioloop.stop()

    def on_channel_open(self, channel: Channel) -> None:
        """Invoked when the channel has been opened"""

        self._channel = channel
        self._channel.add_on_close_callback(self.on_channel_closed)
        self._consumer_tag = self._channel.basic_consume(self._consume_queue, self.on_message, auto_ack=True)

    def on_channel_closed(self, _closed_channel: Channel, reply_code: int) -> None:
        """Invoked when RabbitMQ unexpectedly closes the channel"""

        logger.error(f'Channel has been closed with: {reply_code}')
        self._channel = None
        self._connection.close()

    def publish_response(self, response: Response) -> None:
        """Publish response to appropriate queue"""

        self._channel.basic_publish(
            exchange=self._exchange,
            routing_key=self._publish_queue,
            body=response.to_json().encode('utf-8'),
            properties=pika.BasicProperties(content_type='application/json')
        )

        log_message = f'Following response has been published: {response}'

        if isinstance(response, SuccessfulResponse):
            logger.info(log_message)
        else:
            logger.error(log_message)

    def run(self) -> None:
        """Run client by connecting and then starting the IOLoop"""

        self._connection = self.connect()

        if self._connection:
            try:
                self._connection.ioloop.start()
                logger.info(f'Worker for {self._consume_queue} and {self._publish_queue} is running')
            except AMQPError as e:
                logger.error(f'AMQP exception occurred: {e.args[0]}')
                self.stop()

    def stop(self) -> None:
        """Stop client by closing the channel and connection"""

        logger.info('Stopping client...')

        self.close_channel()
        self.close_connection()

    def close_channel(self) -> None:
        """Close the channel with RabbitMQ"""

        if self._channel is not None:
            self._channel.close()

    def close_connection(self) -> None:
        """Close the connection to RabbitMQ"""

        if self._connection is not None:
            self._connection.close()

    def run_task_in_executor(self, task: Callable, operation_id: str, *args) -> Any:
        """
        Run a task in a thread of internal pool.
        Should not be closed because the event loop used by PikaClient as well.
        Will be used to gather several tasks in the nearest future.
        """

        pool = ThreadPoolExecutor(max_workers=multiprocessing.cpu_count())
        loop = asyncio.get_event_loop()
        future = asyncio.wait_for(loop.run_in_executor(pool, task, *args), timeout=self._model_timeout)

        try:
            result = loop.run_until_complete(future)
        except concurrent.futures.TimeoutError:
            raise DATimeoutException(f'Model timeout has been reached for {operation_id}')
        else:
            return result
