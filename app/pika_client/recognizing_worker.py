from pika import ConnectionParameters

from app.database.redis_store import RedisStore
from app.logger import logger_instance
from app.models.request import Request
from app.models.response import SuccessfulResponse, ResponseResult, ErrorResponse
from app.pika_client.worker import Worker
from app.recognizer.face_recognizer import FaceRecognizer, image_to_numpy_array
from app.validation.exceptions import DATimeoutException, DAStoreException

logger = logger_instance.get_logger(__name__)


class RecognizingWorker(Worker):
    """Worker for recognizing picture"""

    def __init__(
            self,
            con_par: ConnectionParameters,
            consume_queue: str,
            publish_queue: str,
            redis_store: RedisStore,
            recognizer: FaceRecognizer,
            exchange='',
            model_timeout=float('inf'),
            consume_timeout=float('inf')
    ):
        super().__init__(
            con_par=con_par,
            consume_queue=consume_queue,
            publish_queue=publish_queue,
            redis_store=redis_store,
            recognizer=recognizer,
            exchange=exchange,
            model_timeout=model_timeout,
            consume_timeout=consume_timeout
        )

        logger.info(f'Recognizing worker for {self._consume_queue} and {self._publish_queue} has been initialized')

    def handle(self, request: Request) -> None:
        """Recognize picture using request data"""

        operation_id, data = request.operation_id, request.data

        image = image_to_numpy_array(data)

        try:
            recognized_id, _ = self.run_task_in_executor(self._recognizer.recognize, operation_id, image)
        except DATimeoutException as e:
            self._responses.append(ErrorResponse.from_exception(request=request, exc=e))
        else:
            if recognized_id is None:
                self._responses.append(SuccessfulResponse(
                    operation_id=request.operation_id,
                    client_id=ResponseResult.NULL.value,
                    result=ResponseResult.FALSE.value
                ))
            else:
                try:
                    recognized_uuid = self._redis_store.get_recognized_user_uuid(recognized_id)
                except DAStoreException as e:
                    self._responses.append(ErrorResponse.from_exception(request=request, exc=e))
                else:
                    self._responses.append(SuccessfulResponse(
                        operation_id=request.operation_id,
                        client_id=recognized_uuid,
                        result=ResponseResult.TRUE.value
                    ))
