import numpy as np
from pika import ConnectionParameters

from app.database.redis_store import RedisStore
from app.logger import logger_instance
from app.models.request import Request
from app.models.response import SuccessfulResponse, ResponseResult, ErrorResponse
from app.pika_client.worker import Worker
from app.recognizer.face_recognizer import FaceRecognizer, image_to_numpy_array
from app.validation.exceptions import DATimeoutException

logger = logger_instance.get_logger(__name__)


class LearningWorker(Worker):
    """Worker for training model"""

    def __init__(
            self,
            con_par: ConnectionParameters,
            consume_queue: str,
            publish_queue: str,
            redis_store: RedisStore,
            recognizer: FaceRecognizer,
            exchange='',
            model_timeout=float('inf'),
            consume_timeout=float('inf')
    ):
        super().__init__(
            con_par=con_par,
            consume_queue=consume_queue,
            publish_queue=publish_queue,
            redis_store=redis_store,
            recognizer=recognizer,
            exchange=exchange,
            model_timeout=model_timeout,
            consume_timeout=consume_timeout
        )

        logger.info(f'Learning worker for {self._consume_queue} and {self._publish_queue} has been initialized')

    def handle(self, request: Request) -> None:
        """Train existing model using request data"""

        operation_id, client_id, data = request.operation_id, request.client_id, request.data

        image = image_to_numpy_array(data)
        label = self._redis_store.get_mapped_user_id(client_id)

        try:
            self.run_task_in_executor(self._run_learning_controller, operation_id, request, image, label)
        except DATimeoutException as e:
            self._responses.append(ErrorResponse.from_exception(request=request, exc=e))

    def _run_learning_controller(self, request: Request, image: np.ndarray, label: int) -> None:
        """Control training response publishing conditions"""

        self._recognizer.train_recognizer(image, label)

        if request.is_completed() and request.operation_id not in self._recently_removed_tasks:
            self._responses.append(SuccessfulResponse(
                operation_id=request.operation_id,
                client_id=request.client_id,
                result=ResponseResult.TRUE.value
            ))
