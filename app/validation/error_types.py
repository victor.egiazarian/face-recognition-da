from dataclasses import dataclass
from enum import Enum


class ErrorType(Enum):
    CONVERSION = 'CONVERSION_ERROR'
    TIMEOUT = 'TIMEOUT_ERROR'
    QUEUE = 'QUEUE_ERROR'
    RECOGNITION = 'RECOGNITION_ERROR'
    STORE = 'STORE_ERROR'
    CONFIG = 'CONFIG_ERROR'


@dataclass
class ErrorMessage:
    type: ErrorType
    message: str

