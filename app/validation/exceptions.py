from app.logger import logger_instance
from app.validation.error_types import ErrorType

logger = logger_instance.get_logger(__name__)


class DABaseException(Exception):
    """Base class to have DA own Exception hierarchy"""

    error_type: ErrorType

    def __init__(self, *args):
        super().__init__(*args)
        logger.error(f'<{self.error_type}> {self.message}')

    @property
    def message(self) -> str:
        return self.args[0]


class DAConversionException(DABaseException):
    error_type = ErrorType.CONVERSION


class DATimeoutException(DABaseException):
    error_type = ErrorType.TIMEOUT


class DAQueueException(DABaseException):
    error_type = ErrorType.QUEUE


class DARecognitionException(DABaseException):
    error_type = ErrorType.RECOGNITION


class DAStoreException(DABaseException):
    error_type = ErrorType.STORE


class DAConfigException(DABaseException):
    error_type = ErrorType.CONFIG
