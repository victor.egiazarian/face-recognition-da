# Face recognition microservice

Implementation of two types of concurrent RabbitMQ  workers: for training and recognition purposes.


Each worker runs as independent process orchestrated by circus. 
Can be used without circus: every worker should be placed inside of a docker container with docker-compose orchestration over it.  

Recognition feature is driven by OpenCV. Redis database is responsible for mapping of trained and recognized ids to OpenCV ones.    

There are various timeouts introduced in order to ensure reliable and predictable data transfer. Timeouts as well as other microservice's parameters can be specified in launch_worker config.  